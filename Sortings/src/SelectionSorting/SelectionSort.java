// Time Complexity of Selection Sort is o(n2) in both best and worst case.
package SelectionSorting;

import java.util.*;

public class SelectionSort {

	public static void main(String[] args)
	{
		 Scanner cin = new Scanner(System.in);
	        int n,i,j,k,temp=0,min;
	        System.out.print("Enter Number of Elements to Be Inserted:-\n");
			n = cin.nextInt();
			System.out.print("Enter "+n+" Elements:-\n");
			int a[] = new int[n];
			for(k=0;k<n;k++)
	        {
	            a[k]=cin.nextInt();
	        }

	        for(i=0;i<n-1;i++)
	        {
	            min=i;
	            for(j=i+1;j<n;j++)
	            {
	                if(a[j]<a[min])
	                {
	                    min=j;
	                }
	            }
	            if(min!=i)
	            {
	                temp=a[i];
	                a[i]=a[min];
	                a[min]=temp;
	            }
	        }
	        System.out.print("After Sorting Elements Are:- \n");
	        for(i=0;i<n;i++)
	        {
	            System.out.print(a[i]+" ");
	        }
	}

}
