// Bubble Sorting to compare all the elements again & again
package BubbleSorting;
import java.util.*;
public class Sorting1
{
	public static void main(String[] args)
	{
		Scanner cin = new Scanner(System.in);
		int n,i,j,k,temp=0;
		System.out.print("Enter Number of Elements to Be Inserted:-\n");
		n = cin.nextInt();
		System.out.print("Enter "+n+" Elements:-\n");
		int a[] = new int[n];
		for(k=0;k<n;k++)
		{
			a[k] = cin.nextInt();
		}
		
		for(i=0;i<n-1;i++)
		{
			for(j=0;j<n-1;j++)
			{
				if(a[j]>a[j+1])
				{
					temp = a[j];
					a[j] = a[j+1];
					a[j+1] = temp;
				}
			}
		}
		System.out.print("After Sorting Elements Are:- \n");		
		for(i=0;i<n;i++)
		{
			System.out.print(a[i]+" ");
		}

	}
}