//  Time Complexity:-
//  Best & Average Case = O(n log n)
//  Worst Case = O(n2)

package QuickSorting;
import java.util.*;
public class QuickSort 
{
	int partition(int arr[],int low,int high)
	{
		int pivot,i,j,temp1,temp2;
	    pivot = arr[high];
		i = (low-1);
		for(j=low;j<high;j++)
		{
			if(arr[j]<=pivot)
			{
				i++;
				temp1 = arr[i];
				arr[i] = arr[j];
				arr[j] = temp1;
			}
		}
		 
		temp2 = arr[i+1];
		arr[i+1] = arr[high];
		arr[high] = temp2;
		
		return i+1;
	}
	
	
	void sort(int arr[],int low,int high)
	{
		if(low<high)
		{
			int part = partition(arr,low,high);
			sort(arr,low,part-1);
			sort(arr,part+1,high);
		}
	}
	
	public static void main(String[] args) 
	{
	  Scanner cin =  new Scanner(System.in);
	  int n,i,j;
	  System.out.println("Enter Number of Elements: ");
	  n=cin.nextInt();
	  System.out.println("Enter "+n+" Elements: ");
	  int arr[] = new int[n];
	  for(i=0;i<n;i++)
	  {
		  arr[i]=cin.nextInt();
	  }
	  
	  QuickSort obj = new QuickSort();
	  obj.sort(arr,0,n-1);
	  
	  System.out.println("Afer Sorting: ");
	  for(i=0;i<n;i++)
	  {
		  System.out.print(arr[i]+" ");
	  }
	}

}
