//  Time Complexity:-
//  Best , Average & Worst Case = O(n log n)

package MergeSort;
import java.util.*;
public class MergeSort
{
	public static void main(String args[])
	{
		Scanner cin = new Scanner(System.in);
		int n,i;
		System.out.println("Enter Number of Elements:");
		n = cin.nextInt();
		System.out.println("Enter "+n+" Elements:");
		int arr[] = new int[n];
		for(i=0;i<n;i++)
		{
			arr[i] = cin.nextInt();
		}
		
		mergeSort(arr);
		
		System.out.println("After Sorting:");
		for(i=0;i<n;i++)
		{
			System.out.print(arr[i]+" ");
		}
	}
	
	static void mergeSort(int arr[])
	{
		int s,mid,i;
		s = arr.length;
		
		if(s<2)
		{
			return;
		}
		mid = s/2;
		int arr1[] = new int[mid];
		int arr2[] = new int[s-mid];
		
		for(i=0;i<mid;i++)
		{
			arr1[i]= arr[i]; 
		}
		
		
		for(i=mid;i<s;i++)
		{
			arr2[i-mid]= arr[i]; 
		}
		
		mergeSort(arr1);
		mergeSort(arr2);
		merge(arr,arr1,arr2);
	}
	
	static void merge(int arr[],int arr1[],int arr2[])
	{
		int a1l,a2l,i=0,j=0,k=0;
		
	//a1l = array 1 length.....
	//a2l = array 2 length.....
		
		a1l = arr1.length;
		a2l = arr2.length;
		
		while(i<a1l && j<a2l)
		{
			if(arr1[i]<=arr2[j])
			{
				arr[k] = arr1[i];
				i++;
			}
			else
			{
				arr[k] = arr2[j];
				j++;
			}
			k++;
		}
		
		while(i<a1l)
		{
			arr[k]=arr1[i];
			i++;
			k++;
		}
		
		while(j<a2l)
		{
			arr[k]=arr2[j];
			j++;
			k++;
		}
	}
}
