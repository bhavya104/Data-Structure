// Time Complexity = O[d*(n+k)] where d is number of digits each element will have...

package RadixSorting;
import java.util.*;
public class RadixSort
{
	public static void main(String[] args)
	{
		Scanner cin = new Scanner(System.in);
		int n,i;
		System.out.println("Enter Number of Elements :");
		n = cin.nextInt();
		System.out.println("Enter "+n+" Elements :");
		int a[] = new int[n];
		for(i=0;i<n;i++)
		{
			a[i]=cin.nextInt();
		}
		
		radixSort(a,n);
		
		System.out.println("After Sorting Elements :");
		for(i=0;i<n;i++)
		{
			System.out.print(a[i]+" ");
		}
	}
	
	static void radixSort(int a[],int n)
	{
		int max=0,i,pos;
		max=a[0];
		for(i=0;i<n;i++)
		{
			if(max<a[i])
			{
				max = a[i];
			}
		}
		
		for(pos=1; max/pos>0; pos*=10)
		{
			countSort(a,n,pos,max);
		}
	}
	
	static void countSort(int a[],int n,int pos,int max)
	{
	    int i;
		int count[] = new int[max+1];
		int b[] = new int[n];
		
		for(i=0;i<n;i++)
		{
		    ++count[(a[i]/pos)%10];
		}
		
		for(i=1;i<=max;i++)
		{
		    count[i] = count[i]+count[i-1];
		}
		
		for(i=n-1;i>=0;i--)
		{
		    b[--count[(a[i]/pos)%10]] = a[i];
		}
		
		for(i=0;i<n;i++)
		{
		    a[i] = b[i];
		}
	}

}
