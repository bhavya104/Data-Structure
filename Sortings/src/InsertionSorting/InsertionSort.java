package InsertionSorting;

import java.util.*;

public class InsertionSort 
{

	public static void main(String[] args)
	{
		Scanner cin = new Scanner(System.in);
		int n,i,j,k,temp;
		System.out.print("Enter Number of Elements to Be Inserted:-\n");
		n = cin.nextInt();
		System.out.print("Enter "+n+" Elements:-\n");
		int a[] = new int[n];
		for(k=0;k<n;k++)
		{
			a[k] = cin.nextInt();
		}
		
		for(i=1;i<n;i++)
		{
			temp=a[i];
			j=i-1;
			while(j>=0 && a[j]>temp)
			{
				a[j+1]=a[j];
				j--;
			}
			a[j+1]=temp;
		}
		
		System.out.print("After Sorting Elements Are:- \n");		
		for(i=0;i<n;i++)
		{
			System.out.print(a[i]+" ");
		}
	}

}
