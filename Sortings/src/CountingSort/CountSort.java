// This is non-comparison based sorting ie. in this sorting we do not compare elements but we sort
// according to keys.

// Time Complexity = O(n+k)
// Space Complexity = O(n+k)

package CountingSort;
import java.util.*;
public class CountSort
{
	public static void main(String[] args)
	{
		Scanner cin = new Scanner(System.in);
		int n,i,max=0;
		System.out.println("Enter Number of Elements :");
		n=cin.nextInt();
		System.out.println("Enter "+n+" Elements :");
		int a[] = new int[n];
		for(i=0;i<n;i++)
		{
			a[i]= cin.nextInt();
		}
		max=a[0];
		for(i=0;i<n;i++)
		{
			if(max < a[i])
			{
				max = a[i];
			}
		}
		
		countSort(a,n,max);
		
		System.out.println("After Sorting Elements :");
		for(i=0;i<n;i++)
		{
			System.out.print(a[i]+" ");
		}
	}
	
	static void countSort(int a[],int n,int max)
	{
		int i;
		int b[] = new int[n];
		int count[] = new int[max+1];
		
		for(i=0;i<n;i++)
		{
			++count[a[i]];
		}
		
		for(i=1;i<=max;i++)
		{
			count[i] = count[i]+count[i-1];
		}
		
		for(i=n-1;i>=0;i--)
		{
			b[--count[a[i]]] = a[i];
		}
		
		for(i=0;i<n;i++)
		{
			a[i] = b[i];
		}
	}
}